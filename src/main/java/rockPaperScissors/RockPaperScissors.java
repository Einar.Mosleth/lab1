package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    	//Prøver å følge Sondre sin Python løsning og "oversette" den til java
    	//her er run game loopen han lagde, resten av metodene kommer under
    	while (true) {
    		System.out.println("Let's play round " + roundCounter); 
    		String humanChoice = userChoice();
    		String computerChoice = randomChoice();
    		String choiceString = ("Human chose " + humanChoice + ", computer chose " + computerChoice + ".");
    		
    		if (isWinner(humanChoice, computerChoice)) {
    			System.out.println(choiceString + " Human wins.");
    			humanScore += 1;
    		} else if (isWinner(computerChoice, humanChoice)){
    			System.out.println(choiceString + " Computer wins.");
    			computerScore += 1;
    		} else {
    			System.out.println(choiceString + " It's a tie");
    		}
    		
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    		String continueAnswer = continuePlaying();
    		if (continueAnswer.equals("n")) {
    			System.out.println("Bye bye :)");
    			break;
    		} else {
    			roundCounter += 1;
    		}
    	}
    }
     public String randomChoice() {
    	Random rand = new Random();
    	String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
    	return computerChoice;
    }   
     
    public boolean isWinner (String choice1, String choice2) {
    	/* hvorfor fungerer ikke f.eks:
    	 *if (choice1 =="paper") {
    		return (choice2 == "rock");
    	} else if (choice1 =="scissors") {
    		return (choice2 == "paper");
    	} else if (choice1== "rock"){
    		return (choice2 == "scissors");
    	} else
		return false;
		men choice1.equals fungerer?
    	 */
    	
    	if (choice1.equals("paper")) {
    		return (choice2.equals("rock"));
    	} else if (choice1.equals("scissors")) {
    		return (choice2.equals("paper"));
    	} else {
    		return (choice2.equals("scissors"));
    	}
    }
    
    public String userChoice() {
    	while (true) {
    		String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
    		if (rpsChoices.contains(humanChoice)){
    			return humanChoice;
    		} else System.out.println("I do not understand " + humanChoice + ". Could you try again?");
    	}
    }
    
    public String continuePlaying() {
    	while (true) {
    		String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
    		if (continueAnswer.contains("y")) {
    			return continueAnswer;
    		} else if (continueAnswer.contains("n")) {
    			return continueAnswer;
    		} else {
    			System.out.println("I do not understand " + continueAnswer + ". Try again");
    		}
    	}
    }
    
    public String validateInput(String input, String validInput) {
    	input = readInput(input);
    	return readInput(validInput);
    }

	/**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
